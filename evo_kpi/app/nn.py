import pandas as pd

from app.tags import TagContainer

class NeuralNet(TagContainer):
    def __init__(self, tags, equipment):
        for t in tags:
            self.__setattr__(t.tag, t)
        for k, v in equipment.items():
            self.__setattr__(k, v)