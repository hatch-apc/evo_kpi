import logging

LOG_FORMAT = '%(asctime)s | %(levelname)s | %(module)s.%(funcName)s.%(lineno)s: %(message)s'

logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)
logger = logging.getLogger(__name__)