import glob
import pytz
import pandas as pd
import numpy as np

from app.common import logger

DEF_DATASET_DIR = 'evo_kpi\datasets'
DEF_TZ = 'Canada/Mountain'


def build_df(data_dir: str = DEF_DATASET_DIR, tz: str = DEF_TZ):
    tz = pytz.timezone(tz)

    logger.info('Building the dataset!')

    df = pd.DataFrame()  # a new empty dataframe

    for f in glob.glob(data_dir+'/*.csv'):
        logger.info(f'Reading file: {f}')

        # read the raw file and enforce the datatypes/index we would like to see...
        d = pd.read_csv(f, low_memory=False)  # file-specific dataframe
        idx = d.columns[0]  # assume that the index column is the first in the csv file, should be DateTime
        d[idx] = pd.to_datetime(d[idx])  # try converting the first column to datetime
        d.set_index(idx, inplace=True)  # use the first column as our index
        d = d.apply(pd.to_numeric, errors='coerce')  # make the entire dataframe numeric or NaN (e.g. "null" -> NaN)

        # updates on our progress thus far...
        logger.debug(f'...file [{f}] has shape: {d.shape}')
        logger.debug(f'...file [{f}] covers time ranges: {d.index.min()} through {d.index.max()}')

        # try to deal with/localize the time data...
        try:
            d = d.tz_localize(tz, ambiguous='infer', nonexistent='shift_forward')
        except pytz.AmbiguousTimeError as e: # just because we took a sample at 1:30AM on Nov 7...
            logger.warning(f'Assuming DST. {e}')
            assume_dst = np.array([True]* d.shape[0])
            d = d.tz_localize(tz, ambiguous=assume_dst, nonexistent='shift_forward')
        except TypeError as te:
            logger.warning(f'Ignoring TZ for [{f}] (should already be TZ aware). {te}')

        # check for duplicate indices, do this after contextualizing the timestamps (i.e. some dupes expected due to DST...)
        d_idx, d_col = dupe_idx_check(d)
        if len(d_idx) > 0:
            raise IndexError(f'File [{f}] containes duplicate indices. Please clean:\n{d_idx}')
        if len(d_col) > 0:
            raise IndexError(f'File [{f}] containes duplicate columns. Please clean:\n{d_col}')

        df = d.combine_first(df)  # overwrite any of the previous data at the same index with this file, otherwise extend our df
        logger.info(f'DataFrame is now: {df.shape}')
    return df


def dupe_idx_check(df: pd.DataFrame):
    d_idx = df.index[df.index.duplicated()]
    d_col = df.columns[df.columns.duplicated()]
    if len(d_idx) > 0:
        logger.debug(f'Rows with duplicates:\n{d_idx}')
    if len(d_col) > 0:
        logger.debug(f'Columns with duplicates:\n{d_col}')
    return d_idx, d_col
