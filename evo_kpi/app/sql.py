import pytz
import datetime


TZ = pytz.timezone('Canada/Mountain')
START_TIME = datetime.datetime(2021, 11, 15, 0, 0, 0, 0, TZ)
END_TIME = datetime.datetime.now().date()
RESOLUTION = 300000
QUERY_DIR = 'queries'


def gen_sql(tags, start_time=START_TIME, end_time=END_TIME, resolution=RESOLUTION, silent=False):
    query = list()
    query.append('SELECT * FROM OPENQUERY(INSQL, "SELECT DateTime = convert(nvarchar, DateTime, 21)')
    for t in tags:
        query.append(', [%s]' % t)
    query.append("FROM WideHistory")
    query.append("WHERE wwRetrievalMode = 'Cyclic'")
    query.append("AND wwResolution = %i" % resolution)
    query.append("AND wwQualityRule = 'Extended'")
    query.append("AND wwVersion = 'Latest'")
    query.append("AND DateTime >= '%s'" % start_time.strftime('%Y%m%d %H:%M:%S.000'))
    query.append("AND DateTime <= '%s'\")" % end_time.strftime('%Y%m%d %H:%M:%S.000'))
    if not silent:
        for l in query:
            print(l)
    return query


def gen_sql_periods(tags: list, time_ranges: list, resolution=RESOLUTION, file_name=None, silent=False):
    # begin formulating the query
    query = list()
    query.append('SET QUOTED_IDENTIFIER OFF')
    if not isinstance(time_ranges, list):
        raise TypeError(
            'time_ranges must be supplied as a list of tuples: [(start_time1, end_time1), ..., (start_timeN, end_timeN)]'
        )
    for i, r in enumerate(time_ranges):
        if not all(isinstance(t, datetime.datetime) for t in r):
            raise TypeError(
                'start and end times must be supplied as %s' % datetime.datetime)
        if not len(r) == 2:
            raise ValueError('start_time/end_time tuples must be of length 2 (%i supplied)' % len(r))
        if i > 0:  # don't forget to add the union between our query statements
            query.append('UNION')
        query.extend(gen_sql(tags, start_time=r[0], end_time=r[1], resolution=resolution, silent=True))
    if len(time_ranges) > 1:
        query.append('ORDER BY DateTime ASC')

    if not silent:
        for l in query:
            print(l)
    if file_name is not None:
        with open(QUERY_DIR + '/' + file_name, 'w') as f:
            for l in query:
                f.write("%s\n" % l)
    return query

if __name__ == '__main__':
    test_tags = [
        'PF-TPH',
    ]
    gen_sql(test_tags)