import pandas as pd

from app.common import logger

from app.tags import Tag
from app.equipment.equipment import Equipment


class Thickener(Equipment):
    def __init__(self,
            rake_torque: Tag = None,
            rake_height: Tag = None,
            rake_amps: Tag = None,
            bed_pressure: Tag = None,
            level: Tag = None,
            froth_level: Tag = None,
            floc_flow1: Tag = None,
            floc_flow2: Tag = None
            ):
        super().__init__()
        self.rake_torque_tag = rake_torque
        self.rake_height_tag = rake_height
        self.rake_amps_tag = rake_amps
        self.bed_pressure_tag = bed_pressure
        self.level_tag = level
        self.froth_level_tag = froth_level
        self.floc_flow1_tag = floc_flow1
        self.floc_flow2_tag = floc_flow2
    
    def floc_flow(self, df: pd.DataFrame):
        return self.floc_flow1_tag.values(df) + self.floc_flow2_tag.values(df)

    def froth_depth(self, df: pd.DataFrame):
        return self.froth_level_tag.values(df) - self.level_tag.values(df)
    
    def rake_torque(self, df: pd.DataFrame):
        return self.rake_torque_tag.values(df)

    def rake_height(self, df: pd.DataFrame):
        return self.rake_height_tag.values(df)

    def rake_amps(self, df: pd.DataFrame):
        return self.rake_amps_tag.values(df)

    def bed_pressure(self, df: pd.DataFrame):
        return self.bed_pressure_tag.values(df)