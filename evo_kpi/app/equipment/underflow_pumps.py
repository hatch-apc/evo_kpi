import pandas as pd

from app.common import logger

from app.tags import Tag, TagContainer
from app.pid import PID
from app.equipment.equipment import Equipment, Valve


class Pump(Equipment):
    def __init__(self, equip_num: int = None, running: Tag = None, flow: Tag = None, density: Tag = None, power: Tag = None, discharge_valve: Tag = None, recirc_valve: Tag = None, flow_pid: PID = None):
        super().__init__()
        self.running_tag = running
        self.flow_tag = flow
        self.density_tag = density
        self.power_tag = power
        self.discharge_valve_tag = discharge_valve
        self.recirc_valve_tag = recirc_valve
        self.flow_pid = flow_pid

        if equip_num is not None:
            self.tags_from_equip_num(equip_num)
    
    def running(self, df: pd.DataFrame):
        return self.running_tag.values(df).astype(bool)
        # return df[self.running_tag.tag]
    
    def running_idx(self, df: pd.DataFrame):
        return df.index[self.running(df)]

    def flow(self, df: pd.DataFrame):
        return self.flow_tag.values(df)

    def density(self, df: pd.DataFrame):
        return self.density_tag.values(df)
    
    def power(self, df: pd.DataFrame):
        return self.power_tag.values(df)

    def discharge_valve(self, df: pd.DataFrame):
        return self.discharge_valve_tag.values(df)
        # return df[self.discharge_valve_tag.tag]
    
    def recirc_valve(self, df: pd.DataFrame):
        return self.recirc_valve_tag.values(df)
        # return df[self.recirc_valve_tag.tag]

    def discharge_open(self, df: pd.DataFrame):
        return self.discharge_valve(df) == Valve.OPEN.value

    def recirc_closed(self, df: pd.DataFrame):
        return self.recirc_valve(df) == Valve.CLOSED.value

    def discharging(self, df: pd.DataFrame):
        return (self.running(df) & self.discharge_open(df) & self.recirc_closed(df))
    
    def discharging_idx(self, df: pd.DataFrame):
        return df.index[self.discharging(df)]


    def tags_from_equip_num(self, equip_num: int, enforce=False):
        equip_tag = f'PU{equip_num:03}E'
        if (self.running_tag is None) or enforce:
            self.running_tag = Tag(f'EVO_FPLT_PUMP{equip_num:03}E_VFD.Inp_Aux', desc=f'{equip_tag} Running Status')
        if (self.flow_tag is None) or enforce:
            self.flow_tag = Tag(f'EVO_FPLT_{equip_tag}_FIT.Val_EU', desc=f'{equip_tag} Discharge Flow', engunits='GPM')
        if (self.density_tag is None) or enforce:
            self.density_tag = Tag(f'EVO_FPLT_{equip_tag}_DIT.Val_EU', desc=f'{equip_tag} Discharge Density', engunits='s.g.')
        if (self.power_tag is None) or enforce:
            self.power_tag = Tag(f'EVO_FPLT_PUMP{equip_num:03}E_Data.Out_Val0', desc=f'{equip_tag} Drive Output Power', engunits='kW')
        if (self.discharge_valve_tag is None) or enforce:
            self.discharge_valve_tag = Tag(f'EVO_FPLT_{equip_tag}_V1_Valve.Sts_HMIState')
        if (self.recirc_valve_tag is None) or enforce:
            self.recirc_valve_tag = Tag(f'EVO_FPLT_{equip_tag}_V2_Valve.Sts_HMIState')
        if (self.flow_pid is None) or enforce:
            self.flow_pid = PID(pv=self.flow_tag, base_tag=f'EVO_FPLT_PU{equip_num:03}E_FIC01') # no need to grab the pv tag if we have flow tag

class PumpGroup(TagContainer):
    def __init__(self, equip_nums: list[int] = None, pumps: dict[str, Pump] = None):
        self.pumps = pumps
        if equip_nums is not None:
            self.pumps_from_equip_nums(equip_nums)
    
    def total_flow(self, df: pd.DataFrame):
        flows = pd.DataFrame(index=df.index)
        for k, v in self.pumps.items():
            flows[k] = v.flow(df)[v.discharging_idx(df)]
        return flows.sum(axis=1)

    def densities(self, df: pd.DataFrame):
        densities = pd.DataFrame(index=df.index)
        for k, v in self.pumps.items():
            densities[k] = v.density(df)[v.running_idx(df)]
        return densities 

    def avg_density(self, df: pd.DataFrame):
        return self.densities(df).mean(axis=1)

    def total_power(self, df: pd.DataFrame):
        powers = pd.DataFrame(index=df.index)
        for k, v in self.pumps.items():
            powers[k] = v.power(df)[v.discharging_idx(df)]
        return powers.sum(axis=1)

    def cascade(self, df: pd.DataFrame):
        casc = pd.DataFrame(index=df.index)
        for k, v in self.pumps.items():
            casc[k] = v.flow_pid.cascade(df) & v.discharging(df)
        return casc
    
    def pumps_from_equip_nums(self, equip_nums: list[int], enforce = False):
        if self.pumps is None:
            self.pumps = dict()
        for p in equip_nums:
            equip_tag = 'PU{id:03}E'.format(id=p)
            if (equip_tag not in self.pumps) or enforce:
                self.pumps[equip_tag] = Pump(equip_num=p)


