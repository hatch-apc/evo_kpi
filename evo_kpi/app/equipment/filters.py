import pandas as pd

from app.common import logger
from app.tags import Tag, TagContainer
from app.pid import PID
from app.equipment.equipment import Equipment


class Filter(Equipment):
    def __init__(self, equip_num: int = None, running: Tag = None, speed: Tag = None, level: Tag = None, torque: Tag = None, vacuum: Tag = None, level_pid: PID = None):
        super().__init__()
        self.running_tag = running
        self.speed_tag = speed
        self.level_tag = level
        self.torque_tag = torque
        self.vacuum_tag = vacuum
        self.level_pid = level_pid

        if equip_num is not None:
            self.tags_from_equip_num(equip_num=equip_num)
        
    def running(self, df: pd.DataFrame):
        return self.running_tag.values(df).astype(bool)
    
    def speed(self, df: pd.DataFrame):
        return self.speed_tag.values(df)

    def level(self, df: pd.DataFrame):
        return self.level_tag.values(df)

    def torque(self, df: pd.DataFrame):
        return self.torque_tag.values(df)

    def vacuum(self, df: pd.DataFrame):
        return self.vacuum_tag.values(df)

    def operating(self, df: pd.DataFrame):
        return self.running(df) & (self.level(df) > 20) & (self.speed(df) > 20) & (self.vacuum(df) > 5)

    def operating_idx(self, df: pd.DataFrame):
        return df.index[self.operating(df)]

    
    def tags_from_equip_num(self, equip_num: int, enforce=False):
        equip_tag = f'FLTR{equip_num:03}E'
        if (self.running_tag is None) or enforce:
            self.running_tag = Tag(f'Filter{equip_num:1}DriveAux', desc=f'{equip_tag} Rotational Drive Running')
        if (self.speed_tag is None) or enforce:
            self.speed_tag = Tag(f'Filter{equip_num:1}Hertz', desc=f'{equip_tag} Speed', engunits='Hz')
        if (self.level_tag is None) or enforce:
            self.level_tag = Tag(f'Filter{equip_num:1}Level', desc=f'{equip_tag} Level', engunits='%')
        if (self.torque_tag is None) or enforce:
            self.torque_tag = Tag(f'Filter{equip_num:1}Torque', desc=f'{equip_tag} Torque')
        if (self.vacuum_tag is None) or enforce:
            self.vacuum_tag = Tag(f'EVO_FPLT_{equip_tag}_PIT01.Val_EU', desc=f'{equip_tag} Vacuum Pressure', engunits='inH2O')
        if (self.level_pid is None) or enforce:
            self.level_pid = PID(pv=self.level_tag, base_tag=f'EVO_FPLT_{equip_tag}_LIC01') # no need to grab the pv tag if we have level tag



class FilterGroup(TagContainer):
    def __init__(self, equip_nums: list[int] = None, filters: dict[str, Filter] = None):
        self.filters = filters    
        if equip_nums is not None:
            self.filters_from_equip_nums(equip_nums)

    def operating(self, df: pd.DataFrame):
        operating = pd.DataFrame(index=df.index)
        for k, v in self.filters.items():
            operating[k] = v.operating(df)
        return operating
    
    def operating_count(self, df: pd.DataFrame):
        return self.operating(df).sum(axis=1)

    def speeds(self, df: pd.DataFrame):
        speeds = pd.DataFrame(index=df.index)
        for k, v in self.filters.items():
            speeds[k] = v.speed(df)[v.operating_idx(df)]
        return speeds

    def avg_speed(self, df: pd.DataFrame):
        return self.speeds(df).mean(axis=1)
    
    def torques(self, df: pd.DataFrame):
        torques = pd.DataFrame(index=df.index)
        for k, v in self.filters.items():
            torques[k] = v.torque(df)[v.operating_idx(df)]
        return torques

    def avg_torque(self, df: pd.DataFrame):
        return self.torques(df).mean(axis=1)

    def vacuums(self, df: pd.DataFrame):
        vacuum = pd.DataFrame(index=df.index)
        for k, v in self.filters.items():
            vacuum[k] = v.vacuum(df)[v.operating_idx(df)]
        return vacuum

    def avg_vacuum(self, df: pd.DataFrame):
        return self.vacuums(df).mean(axis=1)

    def auto(self, df: pd.DataFrame):
        auto = pd.DataFrame(index=df.index)
        for k, v in self.filters.items():
            auto[k] = v.level_pid.auto(df) & v.operating(df)
        return auto

    def filters_from_equip_nums(self, equip_nums: list[int], enforce = False):
        if self.filters is None:
            self.filters = dict()
        for p in equip_nums:
            equip_tag = 'FLTR{id:03}E'.format(id=p)
            if (equip_tag not in self.filters) or enforce:
                self.filters[equip_tag] = Filter(equip_num=p)






