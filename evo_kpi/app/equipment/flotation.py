import pandas as pd

from app.common import logger

from app.tags import Tag
from app.equipment.equipment import Equipment
from app.tags import TagContainer

class Bank(Equipment):
    def __init__(self, equip_num: int = None, level: Tag = None, frother: Tag = None):
        super().__init__()
        self.level_tag = level
        self.frother_tag = frother
        if equip_num is not None:
            self.tags_from_equip_num(equip_num)
    
    def level(self, df: pd.DataFrame):
        return self.level_tag.values(df)

    def frother(self, df: pd.DataFrame):
        return self.frother_tag.values(df)

    
    # def operating(self)

    
    def tags_from_equip_num(self, equip_num, enforce=False):
        equip_tag = f'FLT{equip_num:03}E'
        if equip_num <= 5:
            if (self.level_tag is None) or enforce:
                self.level_tag = Tag(f'FC{equip_num}Level', desc=f'{equip_tag} Level', engunits='%')
            if (self.frother_tag is None) or enforce:
                self.frother_tag = Tag(f'FC{equip_num}FrotherFM', desc=f'{equip_tag} Frother (MIBC) Dosage', engunits='cc/min')
        else:
            if (self.level_tag is None) or enforce:
                self.level_tag = Tag(f'EVO_FPLT_{equip_tag}_LIC.PV', desc=f'{equip_tag} Level', engunits='%')
            if (self.frother_tag is None) or enforce:
                self.frother_tag = Tag(f'EVO_FPLT_{equip_tag}_FIT.Val_EU', desc=f'{equip_tag} Frother (MIBC) Dosage', engunits='cc/min')
        
    

class Flotation(TagContainer):
    def __init__(self, equip_nums: list[int] = None, banks: dict[str, Bank] = dict(), collector: Tag = None):
        self.banks = banks
        self.collector_tag = collector
        if equip_nums is not None:
            self.tags_from_equip_nums(equip_nums=equip_nums)

    def frother_flows(self, df: pd.DataFrame):
        flows = pd.DataFrame(index=df.index)
        for k, v in self.banks.items():
            flows[k] = v.frother(df)
        return flows 

    def total_frother(self, df: pd.DataFrame):
        return self.frother_flows(df).sum(axis=1)

    def total_collector(self, df: pd.DataFrame):
        return self.collector_tag.values(df)


    def tags_from_equip_nums(self, equip_nums: list[int], enforce=False):
        for b in equip_nums:
            equip_name = f'FLT{b:03}E'
            if (equip_name not in self.banks) or enforce:
                self.banks[equip_name] = Bank(equip_num=b)