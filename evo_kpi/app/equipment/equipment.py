from enum import Enum
from app.common import logger
from app.tags import Tag, TagContainer


class Valve(Enum):
    FAULT = 0
    INTERLOCKED = 1
    OPENING = 2
    OPEN = 3
    CLOSING = 4
    CLOSED = 5

class Equipment(TagContainer):
    all_equip_instances = []

    @classmethod
    def all_equipment_tags(cls):
        tl = []
        for obj in cls.all_equip_instances:
            tl.extend(obj.tags)
        return list(set(tl))       

    def __init__(self):
        Equipment.all_equip_instances.append(self)

    def __del__(self):
        Equipment.all_equip_instances.remove(self)

    # @property
    # def tags(self):
    #     tl = []
    #     for k, v in vars(self).items():
    #         if isinstance(v, Tag):
    #             tl.append(v.tag)
    #     return tl