import types
import pandas as pd

# import chart_studio.plotly as py

import plotly.express as px
import plotly.io as pio
# from plotly.offline import plot, iplot, init_notebook_mode
# import plotly.graph_objects as go
# from plotly.offline import init_notebook_mode


from app.common import logger
from app.tags import Tag

pio.renderers.default = 'notebook'

# init_notebook_mode(connected=True)


class KPI:
    all_kpi_instances = dict()

    def __init__(self, name: str = None, tag: Tag = None, desc: str = None, engunits: str = None, sigdig: int = 3, func: types.FunctionType = None, compare: dict[str, pd.DatetimeIndex] = dict()):
        # managed properties
        self.__name = None
        self.fig = None

        # attribute assignments
        self.name = None
        self.desc = None
        self.engunits = None
        self.sigdig = sigdig
        self.func = None
        self.compare = compare  # a dictionary of datetime indices to compare various cases against (e.g. controls on/off)
        

        # simplify mapping if a tag is provided...
        if tag is not None:
            self.name = tag.tag
            self.desc = tag.desc
            self.engunits = tag.engunits
            self.func = tag.values
        
        # overwrite values if specified to the constructor
        if name is not None:
            self.name = name  # manged by property getter/setter
        if desc is not None:
            self.desc = desc  # KPI description
        if engunits is not None:
            self.engunits = engunits  # Engineering units returned by the "func"
        if func is not None:
            self.func = func  # base function to call (that returns a pandas Series) we wish to evaluate further (inputs are a pandas DataFrame)...

    
    def __del__(self):
        KPI.all_kpi_instances.pop(self.name, None)
            
    
    @property
    def name(self):
        return self.__name
    
    @name.setter
    def name(self, name: str):
        if name in KPI.all_kpi_instances:
            logger.warning(f'KPI [{name}] already exists in the KPIStore, overwriting the old entry...')
        if self.name in KPI.all_kpi_instances:
            KPI.all_kpi_instances[name] = KPI.all_kpi_instances.pop(self.name)
        else:
            if name is None:
                name = 'KPI0'
                for i in range(0, len(KPI.all_kpi_instances)):
                    n = 'KPI{idx}'.format(idx=i)
                    if n not in KPI.all_kpi_instances:
                        name = n
                        break
            KPI.all_kpi_instances[name] = self
        self.__name = name

    
    def values(self, df: pd.DataFrame):
        d = pd.DataFrame(index=df.index)
        for k, v in self.compare.items():
            idx = df.index.intersection(v)
            d[k] = self.func(df.loc[idx])
        return d

    def time_in_state(self, idx: pd.DatetimeIndex):
        tis = pd.DataFrame(index=idx)
        if idx.freq is None:
            raise TypeError('Evaluation DatetimeIndex must have a ".freq" parameter (e.g. evenly spaced timestamps). Use "resample" to normalize')
        for k, v in self.compare.items():
            tis[k] = pd.Series(idx.freq.delta.total_seconds() / (60*60), index=v) # report in hrs units (60 secs to a min, 60 mins to an hr)
        return tis

    def total_time(self, idx: pd.DatetimeIndex):
        return pd.Series(idx.freq.delta.total_seconds() / (60*60), index=idx).sum()
    
    def time_in_state_percent(self, idx: pd.DatetimeIndex):
        return self.time_in_state(idx).sum(axis=0) / self.total_time(idx) * 100

    def time_in_state_plot(self, idx: pd.DatetimeIndex, show=False, show_tis=True):
        tis = self.time_in_state(idx).groupby(by=pd._libs.tslibs.timestamps.Timestamp.date).sum()
        plt_df = tis.melt(ignore_index=False)
        self.fig = px.bar(plt_df, x=plt_df.index, y='value', color='variable', title=self.name)
        y_title = None
        annotation = dict()
        if self.desc is not None:
            y_title = self.desc
        if self.engunits is not None:
            if y_title is None:
                y_title = self.engunits
            else:
                y_title = f'{y_title} ({self.engunits})'
        if y_title is not None:
            self.fig.update_layout(yaxis_title=y_title)
        
        if show_tis: # add time in state if desired to the legend...
            for k, v in self.time_in_state_percent(idx).iteritems():
                s = f'{v:.1f}%'
                if k in annotation:
                    annotation[k] = ', '.join([annotation[k], s])
                else:
                    annotation[k] = s
                    
        if len(annotation) > 0:
            for t in self.fig.data:
                if t.name in annotation:
                    t.name = f'{t.name}\n({annotation[t.name]})'
        
        if show:
            self.fig.show('notebook')
        return self.fig
        # if idx.freq is None:
        #     raise TypeError('Evaluation DatetimeIndex must have a ".freq" parameter (e.g. evenly spaced timestamps). Use "resample" to normalize')
        

    @property
    def states(self):
        return list(self.compare.keys())

    def availability(self, idx: pd.DataFrame, state: str = 'on'):
        tis = self.time_in_state(idx)
        total_t = None
        for s in self.states:
            if total_t is None:
                total_t = tis[s]
            else:
                total_t += tis[s]
        return tis[state] / total_t
    
    def std(self, df: pd.DataFrame):
        return self.values(df).std(axis=0)

    def mean(self, df: pd.DataFrame):
        return self.values(df).mean(axis=0)

    def plot(self, df: pd.DataFrame, show=False, show_tis=True, show_mean=True, show_std=True):
        annotation = dict()
        df_plot = self.values(df).melt(ignore_index=False)
        self.fig = px.scatter(df_plot, x=df_plot.index, y='value', color='variable', title=self.name)
        y_title = None
        if self.desc is not None:
            y_title = self.desc
        if self.engunits is not None:
            if y_title is None:
                y_title = self.engunits
            else:
                y_title = f'{y_title} ({self.engunits})'
        if y_title is not None:
            self.fig.update_layout(yaxis_title=y_title)
        if show_tis: # add time in state if desired to the legend...
            for k, v in self.time_in_state_percent(df.index).iteritems():
                s = f'{v:.1f}%'
                if k in annotation:
                    annotation[k] = ', '.join([annotation[k], s])
                else:
                    annotation[k] = s  
        if show_mean:  # show the mean value if desired in the legend...
            for k, v in self.mean(df).iteritems():
                s = f'avg = {v:.{self.sigdig}f}'
                if k in annotation:
                    annotation[k] = ', '.join([annotation[k], s])
                else:
                    annotation[k] = s         
        if show_std:  # show the standard deviation if desired in the legend...
            for k, v in self.std(df).iteritems():
                s = f'std = {v:.{self.sigdig}f}'
                if k in annotation:
                    annotation[k] = ', '.join([annotation[k], s])
                else:
                    annotation[k] = s
        if len(annotation) > 0:
            for t in self.fig.data:
                if t.name in annotation:
                    t.name = f'{t.name}\n({annotation[t.name]})'
        if show:
            self.fig.show('notebook')
        return self.fig
