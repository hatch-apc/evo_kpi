from enum import Enum
import pandas as pd

from app.tags import Tag, TagContainer

class PID_Mode(Enum):
    OPERATOR_AUTO = 0
    OPERATOR_MANUAL = 1
    OPERATOR_CASRAT = 2
    OPERATOR_OVERRIDE = 3
    OPERATOR_HAND = 4
    PROGRAM_AUTO = 10
    PROGRAM_MANUAL = 11
    PROGRAM_CASRAT = 12
    PROGRAM_OVERRIDE = 13
    PROGRAM_HAND = 14


class PID(TagContainer):
    all_pid_instances = []

    @classmethod
    def all_pid_tags(cls):
        tl = []
        for obj in cls.all_pid_instances:
            tl.extend(obj.tags)
        return list(set(tl))

    def __init__(self, pv: Tag = None, sp: Tag = None, cv: Tag = None, mode: Tag = None, base_tag: str = None):
        self.pv_tag = pv
        self.sp_tag = sp
        self.cv_tag = cv
        self.mode_tag = mode

        if base_tag is not None:
            self.tags_from_base_tag(base_tag=base_tag)

        PID.all_pid_instances.append(self)

    def pv(self, df: pd.DataFrame):
        return self.pv_tag.values(df)

    def sp(self, df: pd.DataFrame):
        return self.sp_tag.values(df)

    def cv(self, df: pd.DataFrame):
        return self.cv_tag.values(df)

    def hand(self, df: pd.DataFrame):
        return self.mode_tag.values(df).isin([PID_Mode.OPERATOR_HAND.value, PID_Mode.PROGRAM_HAND.value])
    
    def hand_idx(self, df: pd.DataFrame):
        return df.index[self.hand(df)]

    def manual(self, df: pd.DataFrame):
        return self.mode_tag.values(df).isin([PID_Mode.OPERATOR_MANUAL.value, PID_Mode.PROGRAM_MANUAL.value])
    
    def manual_idx(self, df: pd.DataFrame):
        return df.index[self.manual(df)]

    def auto(self, df: pd.DataFrame):
        return self.mode_tag.values(df).isin([PID_Mode.OPERATOR_AUTO.value, PID_Mode.PROGRAM_AUTO.value])
    
    def auto_idx(self, df: pd.DataFrame):
        return df.index[self.auto(df)]

    def cascade(self, df: pd.DataFrame):
        return self.mode_tag.values(df).isin([PID_Mode.OPERATOR_CASRAT.value, PID_Mode.PROGRAM_CASRAT.value])
    
    def cascade_idx(self, df: pd.DataFrame):
        return df.index[self.cascade(df)]

    def operator(self, df: pd.DataFrame):
        return self.mode_tag.values(df).isin([PID_Mode.OPERATOR_HAND.value, PID_Mode.OPERATOR_MANUAL.value, PID_Mode.OPERATOR_AUTO.value, PID_Mode.OPERATOR_CASRAT.value])
    
    def operator_idx(self, df: pd.DataFrame):
        return df.index[self.operator(df)]

    def program(self, df: pd.DataFrame):
        return self.mode_tag.values(df).isin([PID_Mode.PROGRAM_HAND.value, PID_Mode.PROGRAM_MANUAL.value, PID_Mode.PROGRAM_AUTO.value, PID_Mode.PROGRAM_CASRAT.value])
    
    def program_idx(self, df: pd.DataFrame):
        return df.index[self.program(df)]

    def error(self, df: pd.DataFrame):
        return self.pv_tag.values(df) - self.sp_tag.values(df)

    def tags_from_base_tag(self, base_tag: str, enforce=False):
        if (self.pv_tag is None) or enforce:
            self.pv_tag = Tag(f'{base_tag}.PV', desc='Process Value')
        if (self.sp_tag is None) or enforce:
            self.sp_tag = Tag(f'{base_tag}.SP', desc='Setpoint')
            self.sp_tag.engunits = self.pv_tag.engunits
        if (self.cv_tag is None) or enforce:
            self.cv_tag = Tag(f'{base_tag}.CV', desc='Control Variable', engunits='%')
        if (self.mode_tag is None) or enforce:
            self.mode_tag = Tag(f'{base_tag}.Mode', desc='Controller Mode Indication')