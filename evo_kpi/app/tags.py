import pandas as pd
import numpy as np

import plotly.express as px

from app.common import logger

class Tag:
    all_tag_instances = dict()  # the TagStore

    @classmethod
    def all_tags(cls):
        return list(cls.all_tag_instances.keys())
        

    def __init__(self, tag, desc: str = None, engunits: str = None, min_thres: float = None, max_thres: float = None):
        # managed properties
        self.__tag = None

        # set all other tag attributes
        self.desc = desc
        self.engunits = engunits
        self.min_thres = min_thres
        self.max_thres = max_thres
        
        self.tag = tag  # via the setter to manage the TagStore

    @property
    def tag(self):
        return self.__tag

    @tag.setter
    def tag(self, tag):
        if tag in Tag.all_tag_instances:
            logger.debug(f'Tag [{tag}] already exists in the TagStore, ignoring...')
        else:
            if self.tag in Tag.all_tag_instances:
                Tag.all_tag_instances[tag] = Tag.all_tag_instances.pop(self.tag, self)
            else:
                Tag.all_tag_instances[tag] = self
            self.__tag = tag

    def values(self, df: pd.DataFrame):
        s = df[self.tag]
        if self.min_thres is not None:
            s.loc[s < self.min_thres] = np.nan
        if self.max_thres is not None:
            s.loc[s > self.max_thres] = np.nan
        return s

    def plot(self, df: pd.DataFrame, show: bool = True):
        plt_df = self.values(df)
        fig = px.scatter(plt_df, x=plt_df.index, y=plt_df.columns)
        if show:
            fig.show()
        return fig

class TagContainer:
    @property
    def tags(self):
        tl = []
        for k, v in vars(self).items():
            if isinstance(v, Tag):
                tl.append(v.tag)
                continue
            elif isinstance(v, dict):
                for k1, v1 in v.items():
                    if isinstance(v1, Tag):
                        tl.append(v1.tag)
                        continue
                    try:  # not sure why subclass checking is not working -- for now handling with try/except on the .tags property
                        tl.extend(v1.tags)
                        continue
                    except AttributeError as e:
                        logger.warning(f'Property [{k1}: {v1}] {e}')
                continue
            try:   # not sure why subclass checking is not working -- for now handling with try/except on the .tags property
                tl.extend(v.tags)
                continue
            except AttributeError as e:
                logger.warning(f'Property [{k}: {v}] {e}')
        return list(set(tl))

    def values(self, df: pd.DataFrame):
        return df[self.tags]


if __name__ == '__main__':
    x = Tag('dd')
    