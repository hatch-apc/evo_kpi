FROM jupyter/minimal-notebook:latest

ENV APP_OWNER=jovyan
ENV APP_DIR=/home/${APP_OWNER}/notebook

WORKDIR ${APP_DIR}

COPY --chown=${APP_OWNER} requirements.txt .

RUN pip install --no-cache -r requirements.txt

COPY --chown=${APP_OWNER} evo_kpi evo_kpi

WORKDIR ${APP_DIR}/evo_kpi

CMD [ "jupyter", "notebook" ]